package com.moravskiyandriy.poTest;

import com.moravskiyandriy.pageobjects.implementations.GmailPage;
import com.moravskiyandriy.pageobjects.implementations.ReverseActionPopUp;
import com.moravskiyandriy.utils.DriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Stream;

import static org.testng.Assert.assertEquals;

public class DeleteLettersAndReverseActionTest {
    private static final Logger logger = LogManager.getLogger(DeleteLettersAndReverseActionTest.class);
    private static final String DEFAULT_ACCOUNT_ADDRESS ="SeleniumWebzxcTest1@gmail.com";
    private static final String DEFAULT_ACCOUNT_PASSWORD="seleniumwebzxctest1";
    private static final int DEFAULT_QUANTITY_OF_LETTERS_TO_DELETE=3;

    @DataProvider(parallel = true)
    private Iterator<Object[]> users(){
        return Stream.of(
                new Object[]{Optional.ofNullable(getProperties()
                        .getProperty("ACCOUNT_ADDRESS_1"))
                        .orElse(DEFAULT_ACCOUNT_ADDRESS),
                        Optional.ofNullable(getProperties()
                        .getProperty("ACCOUNT_PASSWORD_1"))
                        .orElse(DEFAULT_ACCOUNT_PASSWORD)},
                new Object[]{Optional.ofNullable(getProperties()
                        .getProperty("ACCOUNT_ADDRESS_2"))
                        .orElse(DEFAULT_ACCOUNT_ADDRESS),
                        Optional.ofNullable(getProperties()
                                .getProperty("ACCOUNT_PASSWORD_2"))
                                .orElse(DEFAULT_ACCOUNT_PASSWORD)},
                new Object[]{Optional.ofNullable(getProperties()
                        .getProperty("ACCOUNT_ADDRESS_3"))
                        .orElse(DEFAULT_ACCOUNT_ADDRESS),
                        Optional.ofNullable(getProperties()
                                .getProperty("ACCOUNT_PASSWORD_3"))
                                .orElse(DEFAULT_ACCOUNT_PASSWORD)},
                new Object[]{Optional.ofNullable(getProperties()
                        .getProperty("ACCOUNT_ADDRESS_4"))
                        .orElse(DEFAULT_ACCOUNT_ADDRESS),
                        Optional.ofNullable(getProperties()
                                .getProperty("ACCOUNT_PASSWORD_4"))
                                .orElse(DEFAULT_ACCOUNT_PASSWORD)},
                new Object[]{Optional.ofNullable(getProperties()
                        .getProperty("ACCOUNT_ADDRESS_5"))
                        .orElse(DEFAULT_ACCOUNT_ADDRESS),
                        Optional.ofNullable(getProperties()
                                .getProperty("ACCOUNT_PASSWORD_5"))
                                .orElse(DEFAULT_ACCOUNT_PASSWORD)}
        ).iterator();
    }

    @Test(dataProvider = "users")
    void deleteLettersAndReverseAction(String accountAddress, String accountPassword) {
        int lettersQuantityBeforeDeletion;
        int lettersQuantityAfterDeletion;
        GmailPage gmailPage = new GmailPage();
        gmailPage.goToGmailPage();
        gmailPage.fillLoginField(accountAddress);
        gmailPage.submitLogin();
        gmailPage.fillPasswordField(accountPassword);
        gmailPage.submitPassword();
        gmailPage.clickIncomingLettersButton();
        lettersQuantityBeforeDeletion = gmailPage.countIncomingLetters();
        gmailPage.checkLettersForDeletion(Optional.ofNullable(getProperties()
                .getProperty("QUANTITY_OF_LETTERS_TO_DELETE"))
                .map(Integer::valueOf)
                .orElse(DEFAULT_QUANTITY_OF_LETTERS_TO_DELETE));
        gmailPage.clickDeleteButton();
        ReverseActionPopUp reverseActionPopUp = new ReverseActionPopUp();
        reverseActionPopUp.clickUndoButton();
        gmailPage.clickIncomingLettersButton();
        lettersQuantityAfterDeletion = gmailPage.countIncomingLetters();
        assertEquals(lettersQuantityAfterDeletion,lettersQuantityBeforeDeletion);
    }

    @AfterMethod
    void quitDriver(){
        DriverManager.quitDriver();
        DriverManager.setDriver(null);
    }

    @AfterClass
    void quitDrivers(){

    }

    private static Properties getProperties() {
        Properties prop = new Properties();
        try (InputStream input = DeleteLettersAndReverseActionTest.class
                .getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (NumberFormatException ex) {
            logger.warn("NumberFormatException found.");
        } catch (IOException ex) {
            logger.warn("IOException found.");
        }
        return prop;
    }
}
