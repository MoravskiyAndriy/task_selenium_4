package com.moravskiyandriy.pageobjects.implementations;

import com.moravskiyandriy.pageobjects.AbstractPageObject;
import com.moravskiyandriy.utils.Waiter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class GmailPage extends AbstractPageObject {
    private static final Logger logger = LogManager.getLogger(GmailPage.class);
    @FindBy(css = "input[type='email']")
    private WebElement loginField;
    @FindBy(css = "input[name='password']")
    private WebElement passwordField;
    @FindBy(id = "identifierNext")
    private WebElement submitLoginButton;
    @FindBy(id = "passwordNext")
    private WebElement submitPasswordButton;
    @FindBy(xpath = "//a[contains(@href,'0/#inbox')]")
    private WebElement incomingLettersButton;
    @FindBy(css = "td.oZ-x3.xY")
    private List<WebElement> incomingLettersCheckboxes;
    @FindBy(css = "div.T-I.J-J5-Ji.nX.T-I-ax7.T-I-Js-Gs.mA")
    private WebElement deleteButton;

    public void goToGmailPage(){
        logger.info("going to Gmail Page");
        driver.get("https://mail.google.com/mail");
    }

    public void fillLoginField(String login){
        logger.info("filling Login Field");
        loginField.sendKeys(login);
    }

    public void submitLogin(){
        logger.info("submitting Login");
        Waiter.waitForElementToBeClickable(submitLoginButton,10);
        submitLoginButton.click();
    }

    public void fillPasswordField(String password){
        logger.info("filling Password Field");
        passwordField.sendKeys(password);
    }

    public void submitPassword(){
        logger.info("submitting Password");
        Waiter.waitForElementToBeClickable(submitPasswordButton,10);
        submitPasswordButton.click();
    }

    public void clickIncomingLettersButton(){
        logger.info("going to Incoming Letters");
        try {
            incomingLettersButton.click();
        }catch (org.openqa.selenium.StaleElementReferenceException ex){
            incomingLettersButton.click();
        }
    }

    public void checkLettersForDeletion(int quantity){
        logger.info("checking Letters for deletion");
        if(incomingLettersCheckboxes.size()<3){
            logger.warn("Not enough letters");
        }
        else {
            for(int i=0;i<quantity;i++){
                Waiter.waitForVisibilityOfElement(incomingLettersCheckboxes.get(i),10);
                Waiter.waitForElementToBeClickable(incomingLettersCheckboxes.get(i),10);
                incomingLettersCheckboxes.get(i).click();
            }
        }
    }

    public void clickDeleteButton(){
        logger.info("clicking Delete Button");
        Waiter.waitForElementToBeClickable(deleteButton,10);
        deleteButton.click();
    }

    public int countIncomingLetters(){
        logger.info("getting letters' quantity");
        incomingLettersButton.click();
        return incomingLettersCheckboxes.size();
    }
}
